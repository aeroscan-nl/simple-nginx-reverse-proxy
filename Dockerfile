FROM nginx:1.18.0-alpine
RUN apk add --no-cache gomplate
COPY . /app/
WORKDIR /app
CMD ["/app/start.sh"]
